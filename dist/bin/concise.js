#!/usr/bin/env node

const {execSync} = require('child_process');

const runCmd =  (cmd) => {
  try {
    execSync(cmd, {stdio: 'inherit'});
  } catch (e) {
    console.error(`Error - Failed to execute: ${cmd}`, e);
    return false;
  }
  return true;
}

const repoName = process.argv[2];
const installDepsCmd = `cd ${repoName} && npx yarn`;
const startConcise = `cd ${repoName} && npx yarn go`;
const gitCheckoutCmd = `git clone --depth 1 https://gitlab.com/a4to/create-con.git ${repoName}`;
const makePathApp = `cd ${repoName}/node_modules/react-scripts/config && sed -i 's|resolveApp, 'src/index'),|resolveApp, 'src/app'),|' path.js`;


const log = (color, text) => {
  console.log(`${color}%s${Log.rst}`, text);
};

const Log = {
  fg: { blk: "\x1b[30m", rd: "\x1b[31m",  grn: "\x1b[32m", 
        yel: "\x1b[33m", bl: "\x1b[34m",  mag: "\x1b[35m", 
        cyn: "\x1b[36m", fff: "\x1b[37m",crim: "\x1b[38m" 
      },
  rst: "\x1b[0m", bri: "\x1b[1m",
  dim: "\x1b[2m", unscore: "\x1b[4m",
  bnk: "\x1b[5m", rev: "\x1b[7m",
  hide: "\x1b[8m"
};


log(Log.fg.grn, `[+] Creating Concise Application Base ...`); log(Log.rst);
const checkedOut = runCmd(gitCheckoutCmd);
if (!checkedOut) process.exit(1);


log(Log.fg.yel, `[*] Installing dependencies ...`); log(Log.rst);
const installed = runCmd(installDepsCmd);
if (!installed) process.exit(1);


log(Log.fg.yel, `[*] Changing default index location ...`); log(Log.rst);
const installed = runCmd(installDepsCmd);
if (!installed) process.exit(1);


log(Log.fg.grn, `[+] Successfully Bootstrapped ${repoName}, Congratulations!`); log(Log.rst);
log(Log.fg.mag, `Beginning Deployment...`); log(Log.rst);
const started = runCmd(startConcise);
if (!started) process.exit(1);

log(Log.fg.grn, `Thank you for using Concise!`); log(Log.rst);



