import logo from "../img/logo.svg";
import "../css/Style.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Welcome to Concise!
        </p>
        <a
          className="App-link"
          href="https://concise.cc"
          target="_blank"
          rel="noopener noreferrer"
        >
          website
        </a>
      </header>
    </div>
  );
}

export default App
