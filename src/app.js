import React from 'react';
import ReactDOM from 'react-dom/client';
import './css/Style.css';
import App from './js/index.js';
import reportWebVitals from './js/reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

reportWebVitals()
